require 'json'

##
# This class represents the lunch group assignment service.
# Given an array of employees, create random group lunch
class LunchService

  MIN_PEOPLE_AT_LUNCH = 3
  MAX_PEOPLE_AT_LUNCH = 5

  attr_accessor :employee_list
  attr_reader :optimal, :groups

  def initialize(employee_list)
    @employee_list = employee_list
    @optimal = nil
  end

  def assign_employees
    return false unless self.valid?
    init_groups
    rand_list = @employee_list.clone.shuffle
    group_num = 1
    rand_list.each_with_index do |val, idx|
      @groups["Group #{group_num}"] << val
      group_num += 1
      group_num = 1 if group_num > optimal_group_size
    end
  end

  def display_groups
    @groups.map{|k,v| puts "#{k} - #{v.to_s}"}
  end

  def valid?
    employee_list.size >= MIN_PEOPLE_AT_LUNCH ? true : false
  end

  private

  def init_groups
    @groups = {}
    optimal_group_size.times {|i|  @groups["Group #{(i+1)}"] = []}
  end

  def optimal_group_size
    empl_count = @employee_list.count
    if @employee_list.count < MIN_PEOPLE_AT_LUNCH
      @optimal = 0
    else
      @optimal = [empl_count/MAX_PEOPLE_AT_LUNCH, empl_count/(MAX_PEOPLE_AT_LUNCH-1), empl_count/MIN_PEOPLE_AT_LUNCH].max
    end
    @optimal
  end

end

##
# This class represents the employee db for persistence.
# Provides basic db functionality like open, save, show and add employee
# Once you join a great company, you will never be removed!
class LunchDb

  attr_reader :employees

  def initialize(json_db)
    @filename = json_db
    @db = JSON.parse(File.read(json_db))
    @employees = @db["employees"]
  end

  def add_employee(id, person)
    if @employees.key?(id)
      puts "Person with same id already exists. Not added."
      return false
    end
    @employees[id] = person
  end

  def save
    File.open(@filename,"w") do |f|
      f.write(@db.to_json)
    end
  end

  def employee_payload
    @employees.to_a
  end

  def print
    puts @db
  end
end


#
## Sample Code
db = LunchDb.new("db.json")

ls1 = LunchService.new(db.employee_payload)

ls1.assign_employees
ls1.display_groups

db = LunchDb.new("db1.json")

db.add_employee("0101","Superman")
db.add_employee("0102","Scooby")

db.save

ls2 = LunchService.new(db.employee_payload)

ls2.assign_employees
ls2.display_groups
